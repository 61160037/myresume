import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titlesetion = Container(
      padding: const EdgeInsets.all(30),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text(
                  'Name : Panthitra Sangsuk',
                  style: GoogleFonts.asap(
                      fontWeight: FontWeight.w400, fontSize: 18),
                ),
              ),
              Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'NickName : Tong',
                    style: GoogleFonts.asap(
                        fontSize: 18, fontWeight: FontWeight.w400),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Gender : Female',
                          style: GoogleFonts.asap(
                              fontSize: 18, fontWeight: FontWeight.w400)),
                      Text(' Age : 21 year',
                          style: GoogleFonts.asap(
                              fontSize: 18, fontWeight: FontWeight.w400)),
                      Text('',
                          style: GoogleFonts.asap(
                              fontSize: 18, fontWeight: FontWeight.w400)),
                      Text('',
                          style: GoogleFonts.asap(
                              fontSize: 18, fontWeight: FontWeight.w400))
                    ],
                  ))
            ],
          )),
        ],
      ),
    );
    Widget titleSkill = Container(
      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            Icons.settings_ethernet,
            color: Colors.green[400],
            size: 30,
          ),
          Text(' Computer Skill',
              style: GoogleFonts.amaticSc(
                  fontSize: 30, fontWeight: FontWeight.w900, letterSpacing: 2)),
          // _buildButtonColumn(color, Icons.call, 'CALL'),
        ],
      ),
    );
    Widget iconSkill = Container(
        padding: const EdgeInsets.all(30),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              '../images/css.png',
              width: 50,
              fit: BoxFit.fitHeight,
            ),
            Image.asset(
              '../images/html-5.png',
              width: 50,
              fit: BoxFit.fitHeight,
            ),
            Image.asset(
              '../images/js.png',
              width: 50,
              fit: BoxFit.fitHeight,
            ),
            Image.asset(
              '../images/dart.png',
              width: 100,
              fit: BoxFit.fitHeight,
            ),
            Image.asset(
              '../images/java.png',
              width: 50,
              fit: BoxFit.fitHeight,
            ),
          ],
        ));

    Widget titlePersonaldata = Container(
      padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            Icons.account_circle,
            color: Colors.green[400],
            size: 30,
          ),
          Text(' Personal data',
              style: GoogleFonts.amaticSc(
                  fontSize: 30, fontWeight: FontWeight.w900, letterSpacing: 2)),
          // _buildButtonColumn(color, Icons.call, 'CALL'),
        ],
      ),
    );
    Widget Personaldata = Container(
      padding: const EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Date of Birth : 8 June 1999 ',
            style: GoogleFonts.asap(fontSize: 18, fontWeight: FontWeight.w400),
          ),
          Text(
            'Nationality : Thai ',
            style: GoogleFonts.asap(fontSize: 18, fontWeight: FontWeight.w400),
          ),
          Text(
            'Email : 61160037@go.buu.ac.th ',
            style: GoogleFonts.asap(fontSize: 18, fontWeight: FontWeight.w400),
          ),
        ],
      ),
    );
    Widget titleEducation = Container(
      padding: EdgeInsets.fromLTRB(30, 0, 30, 30),
      child: Row(
        children: [
          Icon(
            Icons.school,
            color: Colors.green[400],
            size: 30,
          ),
          Text(' Education ',
              style: GoogleFonts.amaticSc(
                  fontSize: 30, fontWeight: FontWeight.w900, letterSpacing: 2)),
        ],
      ),
    );
    Widget Education = Container(
      padding: EdgeInsets.fromLTRB(30, 0, 30, 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '2018 - Present : ',
            style: GoogleFonts.asap(fontSize: 18, fontWeight: FontWeight.w600),
          ),
          Text(
            'B.Sc. (Computer Science) --In progress Faculty of Informatics, Burapha University ',
            style: GoogleFonts.asap(fontSize: 18, fontWeight: FontWeight.w400),
          ),
          Text(
            '2017 - 2015 :',
            style: GoogleFonts.asap(fontSize: 18, fontWeight: FontWeight.w600),
          ),
          Text(
            'High school certificate  Chonburi Sukkhabot School  ',
            style: GoogleFonts.asap(fontSize: 18, fontWeight: FontWeight.w400),
          ),
        ],
      ),
    );

    Widget textResume = Container(
      padding: EdgeInsets.all(30),
      child: Text(
        'RESUME',
        style: GoogleFonts.amaticSc(
            fontSize: 50, fontWeight: FontWeight.bold, letterSpacing: 1.5),
        softWrap: true,
      ),
      alignment: Alignment.center,
    );

    return MaterialApp(
      title: 'Resume Panthitra',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Resume'),
        ),
        body: ListView(
          children: [
            textResume,
            Image.asset(
              '../images/tong.jpg',
              width: 150,
              height: 300,
              fit: BoxFit.fitHeight,
            ),
            titlesetion,
            titleSkill,
            iconSkill,
            titlePersonaldata,
            Personaldata,
            titleEducation,
            Education
          ],
        ),
      ),
    );
  }
}
